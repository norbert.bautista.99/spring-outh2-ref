package com.example.referencesec;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;

@SpringBootApplication
@EnableMongoRepositories
public class ReferenceSecApplication {

	public static void main(String[] args) {
		SpringApplication.run(ReferenceSecApplication.class, args);
	}

}
