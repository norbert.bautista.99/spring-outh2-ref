package com.example.referencesec.controller;

import com.example.referencesec.entity.Book;
import com.example.referencesec.repository.BookRepository;
import com.example.referencesec.service.SequenceGeneratorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

import static com.example.referencesec.entity.Book.SEQUENCE_NAME;

@RestController
public class BookConrtoller {

    @Autowired
    private BookRepository bookRepository;

    @Autowired
    private SequenceGeneratorService service;

    @PostMapping("/saveBook")
    public Book save(@RequestBody Book book) {
        //generate sequence
        book.setId(service.getSequenceNumber(SEQUENCE_NAME));
        return bookRepository.save(book);
    }
    @GetMapping("/getBooks")
    public List<Book> getBooks() {
        return bookRepository.findAll();
    }
}
